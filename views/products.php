<?php include('layouts/header.php') ?>
<?php include('layouts/navbar.php') ?>
</header>
<div class="products bg-light">
    <div class="container">
        <div class="row">
            <?php foreach ($products as $product): ?>
                <div class="col-md-4">
                    <div class="card mb-4 box-shadow">
                        <img class="card-img-top"
                             data-src=""
                             alt="Thumbnail [100%x225]"
                             src="../assets/img/<?= $product->getImage(); ?>"
                             data-holder-rendered="true">
                        <div class="card-body py-5">
                            <h5 class="card-title"><a href="/product/<?= $product->getId(); ?>"> <?= $product->getTitle(); ?></a></h5>
                            <p class="card-text"> <?= $product->getDescription() ?>
                            </p>
                            <div class="d-flex justify-content-between align-items-center">
                                <a href="/category/<?= $product->getCategoryId() ?>" <small class="font-weight-bold"><?= $product->getCategoryTitle() ?></small><a/>
                                <small class="font-weight-bold"><?= $product->getPrice() ?>€</small>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
</body>
</html>
