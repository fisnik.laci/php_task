<div class="container-fluid bg-purple-light py-2">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand d-lg-none" href="#">Menu</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <?php foreach ($categories as $category): ?>
                    <li class="nav-item py-2 py-lg-0 text-center">
                        <a class="nav-link custom-nav-link <?= \core\Helpers::get_last_param() == $category->getId() ? 'active' : ''; ?>" href="/category/<?= $category->getId() ?>"><?= $category->getTitle() ?></a>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </nav>

    </div>
</div>
