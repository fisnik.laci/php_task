<?php include ('views/layouts/header.php')?>
</header>
<div class="container product-details">
    <div class="row py-5">
        <div class="col-md-5 offset-md-2 pt-3 order-md-2">
            <div class="product-img">
                <img src="/assets/img/<?php echo $product['image'] ?>"
                     alt="product">
            </div>
        </div>
        <div class="col-md-5 order-md-1">
            <h3><?php echo $product['title'] ?></h3>
            <p class="mb-3 text-muted text-uppercase small">Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
            <p><span class="mr-1 mb-3"><strong>$12.99</strong></span></p>
            <button type="button" class="btn btn-primary mb-4">Add to Cart</button>
            <p><span class="mr-1"><strong>Category 1</strong></span></p>
            <p class=""><?php echo $product['description'] ?></p>
        </div>
    </div>
</div>
</body>
</html>
