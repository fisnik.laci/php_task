<?php

namespace database;

use Exception;
use PDO;
use PDOException;


class Database
{
    const DSN = 'mysql:host=localhost;dbname=phptask';

    const USERNAME = 'root';

    const PASSWORD = '';

    protected $connection;

    public function __construct()
    {
        $this->connect();
    }

    public function getConnection() {
        return $this->connection;
    }

    public function connect() {
        try {
            $this->connection = new PDO(self::DSN, self::USERNAME, self::PASSWORD);
        } catch (PDOException $e) {
            echo 'error:' . $e->getMessage();
            die();
        }
    }

    /**
     * @author Fisnik Laci
     * @param $query
     * @param $table_name
     * @throws Exception
     * Please be careful. Table will be deleted if exsits
     */
    public function migrate($query, $table_name) {
        $delete_sql = "DROP TABLE ".$table_name." ";
        $this->connection->query($delete_sql);
        $result = $this->connection->query($query);
        if ($result === false) {
            throw new Exception('Something went wrong. Please check query and try again', 404);
        }
    }
}
