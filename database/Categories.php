<?php


namespace database;


class Categories extends Database
{

    /**
     * @var $id
     */
    private $id;

    /**
     * @var $title
     */
    private $title;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    public function exchange($result = [])
    {
        $this->id = $result['id'] ?? null;
        $this->title = $result['title'] ?? null;
    }

    /**
     * @throws \Exception
     * @author Fisnik Laci
     * creates categories table
     */
    public function createTable()
    {
        $sql = "CREATE TABLE categories (
                ID int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                Name varchar(255) NOT NULL,
                created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                updated_at 
                TIMESTAMP DEFAULT CURRENT_TIMESTAMP 
                ON UPDATE CURRENT_TIMESTAMP
                )";
        $this->migrate($sql, 'categories');
    }

    /**
     * @return mixed
     * @author Fisnik Laci
     * returns all categories
     */
    public function all()
    {
        $query = "SELECT * FROM categories";
        $sql = $this->connection->prepare($query);
        $executed = $sql->execute();
        if ($executed) {
            $results = $sql->fetchall(\PDO::FETCH_ASSOC);
            $categories = [];
            foreach ($results as $result) {
                $category = new Categories();
                $category->exchange($result);
                $categories[] = $category;
            }
            return $categories;
        }
    }

    /**
     * @param $id
     * @return mixed
     * @author Fisnik Laci
     * returns category by given id
     */
    public function get_by_id($id)
    {
        $query = "SELECT * FROM categories WHERE ID = :id";
        $sql = $this->connection->prepare($query);
        $executed = $sql->execute(
            array(
                ':id' => $id,
            )
        );
        if ($executed) {
            return $sql->fetch();
        }
    }

}