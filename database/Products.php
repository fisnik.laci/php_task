<?php

namespace database;

use database\Database;
use Exception;

class Products extends Database
{
    /**
     * @var $id
     */
    private $id;

    /**
     * @var $title
     */
    private $title;

    /**
     * @var $description
     */
    private $description;

    /**
     * @var $price
     */
    private $price;

    /**
     * @var $image
     */
    private $image;

    /**
     * @var $category_id
     */
    private $category_id;

    /**
     * @var $category_title
     */
    private $category_title;

    /**
     * Products constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function exchange($result = [])
    {
        $this->id = $result['id'] ?? null;
        $this->title = $result['title'] ?? null;
        $this->price = $result['price'] ?? null;
        $this->description = $result['description'] ?? null;
        $this->image = $result['image'] ?? null;
        $this->category_id = $result['category_id'] ?? null;
        $this->category_title = $result['category_title'] ?? null;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image): void
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getCategoryId()
    {
        return $this->category_id;
    }

    /**
     * @param mixed $category_id
     */
    public function setCategoryId($category_id): void
    {
        $this->category_id = $category_id;
    }

    /**
     * @return mixed
     */
    public function getCategoryTitle()
    {
        return $this->category_title;
    }

    /**
     * @param mixed $category_title
     */
    public function setCategoryTitle($category_title): void
    {
        $this->category_title = $category_title;
    }

    /**
     * @throws Exception
     * @author Fisnik Laci
     * creates products table
     */
    public function createTable()
    {
        $sql = "CREATE TABLE products (
                ID int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                Title varchar(255) NOT NULL,
                Description varchar(255),
                Price varchar(255),
                Image varchar(255),
                Category_ID int(11),
                created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP 
                ON UPDATE CURRENT_TIMESTAMP,
                FOREIGN KEY (Category_ID) REFERENCES categories(ID)
                )";
        $this->migrate($sql, 'products');
    }

    /**
     * @return mixed
     * returns all products
     * @author Fisnik Laci
     */
    public function all($category_id = null)
    {
        if (isset($category_id) && !empty($category_id)) {
            $query = "SELECT p.id, p.title, p.description, p.image, p.price, p.category_id, c.title 
                        AS category_title FROM products AS p
                        LEFT JOIN categories AS c ON c.id = p.category_id WHERE category_id = :category_id";
            $sql = $this->connection->prepare($query);
            $executed = $sql->execute(
                array(
                    ':category_id' => $category_id,
                )
            );
        }else{
            $query = "SELECT p.id, p.title, p.description, p.image, p.price, p.category_id, c.title AS category_title FROM products AS p
                   LEFT JOIN categories AS c ON c.id = p.category_id ";
            $sql = $this->connection->prepare($query);
            $executed = $sql->execute();
        }
        if ($executed) {
            $results = $sql->fetchall(\PDO::FETCH_ASSOC);
            $products = [];
            foreach ($results as $result) {
                $product = new Products();
                $product->exchange($result);
                $products[] = $product;
            }
            return $products;
        }
    }

    /**
     * @param $id
     * @return mixed
     * returns product by a given ID
     * @author Fisnik Laci
     */
    public function get_by_id($id)
    {
        $query = "SELECT * FROM products WHERE ID = :id";
        $sql = $this->connection->prepare($query);
        $executed = $sql->execute(
            array(
                ':id' => $id,
            )
        );
        if ($executed) {
            return $sql->fetch();
        }
    }

    /**
     * @param $title
     * @return mixed
     * @author Fisnik Laci
     */
    public function get_by_name($title)
    {
        $query = "SELECT * FROM articles WHERE Title = :title";
        $sql = $this->connection->prepare($query);
        $executed = $sql->execute(
            array(
                ':title' => $title,
            )
        );
        if ($executed) {
            return $sql->fetch();
        }
    }
}