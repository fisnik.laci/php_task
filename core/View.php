<?php


namespace core;


class View
{
    private $data = array();

    private $render = FALSE;

    private $file;

    public function __construct($template)
    {
        try {
            $file = $_SERVER['DOCUMENT_ROOT'] . '/views/' . strtolower($template) . '.php';

            if (file_exists($file)) {
                $this->file = $file;
            } else {
                throw new \Exception('Template ' . $template . ' not found!');
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function render()
    {
        $this->render = $this->file;
    }

    /**
     * @param $variable
     * @param $value
     * pass data to view
     * @author Fisnik Laci
     */
    public function assign($variable, $value)
    {
        $this->data[$variable] = $value;
    }

    public function __destruct()
    {
        extract($this->data);
        include($this->render);
    }
}