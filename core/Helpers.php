<?php


namespace core;


class Helpers
{
    public function __construct()
    {

    }

    /**
     * @param $path
     */
    public static function redirect($path)
    {
        header('Location: ' . $path . '');
        exit();
    }

    /**
     * @return mixed|null
     * get last param from uri
     */
    public static function get_last_param()
    {
        $url = $_SERVER['REQUEST_URI'];
        $url = explode('/', $url);
        $lastPart = array_pop($url);
        return $lastPart;
    }
}