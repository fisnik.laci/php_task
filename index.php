<?php
include 'autoload.php';

use controllers\Controller;
use controllers\ProductsController;
use core\Helpers;

/**
 * First, let's define our Router object.
 */
class Router
{
    /**
     * The request we're working with.
     *
     * @var string
     */
    public $request;

    /**
     * The $routes array will contain our URI's and callbacks.
     * @var array
     */
    public $routes = [];

    /**
     * @var string
     * stores product name
     */
    public $product;

    /**
     * For this example, the constructor will be responsible
     * for parsing the request.
     *
     * @param array $request
     */
    public function __construct(array $request)
    {
        /**
         * This is a very (VERY) simple example of parsing
         * the request. We use the $_SERVER superglobal to
         * grab the URI.
         */
        //$this->request = basename($request['REQUEST_URI']);
        $this->request = $_SERVER['REQUEST_URI'];
    }

    /**
     * Add a route and callback to our $routes array.
     *
     * @param string $uri
     * @param Callable $fn
     */
    public function addRoute(string $uri, \Closure $fn): void
    {
        $this->routes[$uri] = $fn;
    }

    /**
     * Determine is the requested route exists in our
     * routes array.
     *
     * @param string $uri
     * @return boolean
     */
    public function hasRoute(string $uri): bool
    {
        return array_key_exists($uri, $this->routes);
    }

    /**
     * Run the router.
     *
     * @return mixed
     */
    public function run()
    {
        if ($this->hasRoute($this->request)) {
            $this->routes[$this->request]->call($this);
        } else {
            $controller = new Controller();
            $controller->error();
        }
    }

}

/**
 * Create a new router instance.
 */
$router = new Router($_SERVER);

/**
 * Add routes.
 */
$router->addRoute('/', function () {
    $product = new ProductsController();
    $product->products();
});

$url = $_SERVER['REQUEST_URI'];
$url = explode('/', $url);
$controller = $url[1];
$p = array_pop($url);
if ($controller == 'product') {
    $router->addRoute($_SERVER['REQUEST_URI'], function () use ($p) {
        $product = new ProductsController();
        $product->product_details($p);
    });
}

if ($controller == 'category') {
    $router->addRoute($_SERVER['REQUEST_URI'], function () use ($p) {
        $product = new ProductsController();
        $product->product_category($p);
    });
}

/**
 * Run it!
 */
$router->run();