<?php


namespace controllers;

use core\View;
use database\Categories;
use database\Products;
use mysql_xdevapi\Collection;

class ProductsController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @author Fisnik Laci
     * @return all products
     */
    public function products()
    {
        $prod = new Products();
        $products = $prod->all();
        $cat = new Categories();
        $categories = $cat->all();
        $view = new View('products');
        $view->assign('products', $products);
        $view->assign('categories', $categories);
        $view->render();
    }

    /**
     * @param $name
     * @return a single product details
     * @author Fisnik Laci
     */
    public function product_details($name)
    {
        $product = new Products();
        $res = $product->get_by_id($name);
        $view = new View('product');
        $view->assign('product', $res);
        $view->render();
    }

    /**
     * @param $cat
     * @return products by category
     * @author Fisnik Laci
     */
    public function product_category($category_id)
    {
        $prod = new Products();
        $products = $prod->all($category_id);
        $c = new Categories();
        $categories = $c->all();
        $view = new View('products');
        $view->assign('products', $products);
        $view->assign('categories', $categories);
        $view->render();
    }
}