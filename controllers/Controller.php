<?php

namespace controllers;

class Controller
{
    public function __construct()
    {

    }

    public function error()
    {
        echo 'Something went wrong.';
        die();
    }
}